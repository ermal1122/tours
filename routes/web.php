<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\SocialController;
use App\Http\Controllers\UploadController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::get('/destinations',function(){
//     return view('test');
// });
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::get('about', function () {
    return view('test');
})->middleware('admin');


Route::get('/', [HomeController::class, 'index'])->name('home.index');
Route::get('/post/{id}', [HomeController::class, 'show'])->name('home.show');
Route::post('/payment/pay', [PaymentController::class, 'pay'])->name('pay');
Route::get('/pay', [PaymentController::class, 'index'])->name('pay.index');

Route::get('/checkout/{id}/{title}', [CheckoutController::class, 'index'])->name('home.checkout');

Route::get('/successful/checkout/{payment_id}', [CheckoutController::class, 'success'])->name('checkout.success');
Route::get('/download/pdf/invoice/{payment}', [CheckoutController::class, 'generatePdf'])->name('downloadInvoice');


Route::get('lang/{locale}', function ($locale) {
    session()->put('locale', $locale);
    return redirect()->back();
});

Route::group(['middleware' => ['user']], function () {
    Route::get('/user/account', [UserController::class, 'account'])->name('user.account');
    Route::get('/user/purchases', [UserController::class, 'purcharses'])->name('user.bookings');
    Route::get('/user/change/password', [UserController::class, 'password'])->name('user.changePassword');
    Route::get('/user/edit', [UserController::class, 'edit'])->name('user.edit');

    Route::patch('/user/update/{user}', [UserController::class, 'update'])->name('user.update');
    Route::patch('/user/password/update', [UserController::class, 'changePassword'])->name('user.updatePassword');
});

Route::group(['middleware' => ['admin']], function () {
    Route::get('/admin/dashboard', [PostController::class, 'adminIndex'])->name('admin.adminIndex');
    Route::get('/admin/posts', [PostController::class, 'index'])->name('admin.index');
    Route::get('/admin/post/{id}', [PostController::class, 'show'])->name('admin.show');
    Route::get('/admin/create', [PostController::class, 'create'])->name('post.create');
    Route::post('/admin/upload', [UploadController::class, 'store'])->name('upload.image');
    Route::post('/admin/store', [PostController::class, 'store'])->name('post.store');
    Route::put('/admin/update/{id}', [PostController::class, 'update'])->name('post.update');
    Route::patch('/admin/restore', [PostController::class, 'restore'])->name('post.restore');
    Route::delete('/admin/softdelete/{id}', [PostController::class, 'softDelete'])->name('post.softDelete');
    Route::delete('/admin/gallery/delete/{id}', [PostController::class, 'galleryDelete'])->name('post.galleryDelte');
});

Route::post('/payment', [PaymentController::class, 'payWithpaypal'])->name('paypal.pay');
Route::get('/payment/done', [PaymentController::class, 'getPaymentStatus'])->name('status');

Route::get('login/{provider}', [SocialController::class, 'redirectToProvider'])->name('social.login');
Route::get('login/{provider}/callback', [SocialController::class, 'handleProviderCallback']);
