<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'mail_not_set' => 'The email was not send, please try again later or contact via live chat',
    'mail_send_successfully' => 'The email was delivered, we will get back to you very soon',

    'title' => 'Title',
    'short_descriptoin' => 'Short Descriptoin',
    'created_at' => 'Created At',
    'web_title' => 'Web Title',
    'short_description' => 'Short Description',
    'cover' => 'Cover',
    'banner' => 'Banner',
    'choose_cover' => 'Choose Cover',
    'choose_banner' => 'Choose Banner',
    'content' => 'Content',
    'save_blog' => 'Save Blog',
    'new_blog' => 'New Blog',
    'update_blog' => 'Update Blog',
    'field_is_required' => 'Field is required',
    'actions' => 'Actions',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'our_articles' => 'Our Articles',
    'categories' => 'Categories',
    'tags' => 'Tags',
    'recent_post' => 'Recent Post',
    'team' => 'Team',
    'service' => 'Service',
    'startup' => 'Startup',
    'business' => 'Business',
    'outstaff' => 'Outstaff',
    'new_service' => 'New Service',
    'new_technology' => 'New Technology',
    'choose_image' => 'Choose image',
    'save_technology' => 'Save Technology',
    'image' => 'Image',
    'update_technology' => 'Update Technology',
    'technology' => 'Technology',
    'back' => 'Back',
    'save_service' => 'Save Service',
    'update_service' => 'Update Service',
    'back' => 'Back',

];