<form id="paymentsubmit" method="post" action="{{route('pay')}}">
  @csrf
  <input type="text" name="name">
  <input type="text" placeholder="card number" id="ccNo" name="ccNo">
  <input type="text" placeholder="cvv" name="cvv" id="cvv">
  <input type="text" placeholder="month" name="expMonth" id="expMonth">
  <input type="text" placeholder="year" name="expYear" id="expYear">
  <input type='hidden' name='demo' value='Y' />
  <input hidden type="text" id="token" name="token">
  <button type="submit">Submit</button>
  <p id="to">CLICK</p>
</form>


PAYPAL
<form class="w3-container w3-display-middle w3-card-4 " method="POST" id="payment-form" action="{{route('paypal.pay')}}">
  {{ csrf_field() }}
  <h2 class="w3-text-blue">Payment Form</h2>
  <p>Demo PayPal form - Integrating paypal in laravel</p>
  <p>
    <label class="w3-text-blue"><b>Enter Amount</b></label>
    <input class="w3-input w3-border" name="amount" type="text"></p>
  <button class="w3-btn w3-blue">Pay with PayPal</button></p>
</form>

<script type="text/javascript" src="https://www.2checkout.com/checkout/api/2co.min.js"></script>


<script src="{{asset('assets/vendors/jquery.min.js')}}"></script>



<script>
  var successCallback = function(data) {
    alert(data)

    var myForm = document.getElementById('paymentsubmit');

    myForm.token.value = data.response.token.token;
    myForm.submit();
  }


  // Called when token creation fails.
  var errorCallback = function(data) {
    alert("3")

    if (data.errorCode === 200) {
      // This error code indicates that the ajax call failed. We recommend that you retry the token request.
    } else {
      alert(data.errorMsg);
    }
  };



  var tokenRequest = function() {
    var args = {
      sellerId: "250535517940",
      publishableKey: "4032342B-300E-4801-9808-60701A809657",
      ccNo: $("#ccNo").val(),
      cvv: $("#cvv").val(),
      expMonth: $("#expMonth").val(),
      expYear: $("#expYear").val(),

    };

    TCO.loadPubKey('production', function() {
      TCO.requestToken(successCallback, errorCallback, args);
    })

    // TCO.requestToken(successCallback, errorCallback, args);


  }

  $(function() {
    // Pull in the public encryption key for our environment
    // TCO.loadPubKey('production');

    $("#paymentsubmit").submit(function(e) {

      // Call our token request function
      tokenRequest();

      // Prevent form from submitting
      return false;
    });
  });
</script>