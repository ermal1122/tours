@extends('layouts.client')

@section('content')


<div class="row mt-4 container mx-auto">
    @include('user.sidebar_tabs')
    <div class="col-lg-7 col-sm12 p-4 bg-light">
        @include('notification')
        <div class="tab-content">
            <div id="menu2" class=""><br>
                <h3>Account</h3>
                <form action="{{route('user.updatePassword')}}" method="POST">
                    @csrf
                    @method('PATCH')
                    <div class="form-group">
                        <label for="email">Email:</label>
                        <input type="email" class="form-control" disabled value="{{Auth::user()->email}}" id="email">
                    </div>
                    <div class="form-group">
                        <label for="pwd">Current Password:</label>
                        <input type="password" class="form-control" name="current_password" placeholder="Current Password" id="pwd">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="new_password" placeholder="New Password" id="newpwd">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="new_confirm_password" placeholder="Confirm New Password" id="confirmnewpwd">
                    </div>
                    <button type="submit" class="btn btn-primary float-right">Save Changes</button>
                </form>
            </div>

        </div>
    </div>
</div>
</div>
@endsection