@extends('layouts.client')

@section('content')


<div class="row mt-4 container mx-auto">
    @include('user.sidebar_tabs')
    <div class="col-lg-7 col-sm12 p-4 bg-light">
        <div class="tab-content">
            <div class=""><br>
                <h3>Bookings</h3>
                @foreach($trips as $trip)
                <a href="#">
                    <div class="row rounded bg-white p-2 mt-2">
                        <div class="col-lg-10 col-sm-10">
                            <div class="row">
                                <div class="col-4">
                                    <img src="{{asset('upload/posts') . '/' . $trip->post->profile_photo}}" width="120" height="80" alt="">
                                </div>
                                <div class="col-8">
                                    <span class="font-weight-bold">{{$trip->post->profile_photo}}</span>
                                    <span class="d-block">6 days</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-2 justify-content-center align-self-center">
                            <span class="">${{$trip->price}}</span>
                        </div>
                    </div>
                </a>
                @endforeach
            </div>
        </div>
        <div class="mt-3">
            {{ $trips->links() }}

        </div>

    </div>
</div>
@endsection


<style>
    .sm:justify-between {
        display: none;
    }
</style>