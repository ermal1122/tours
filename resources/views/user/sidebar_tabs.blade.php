<div class="col-lg-5 col-sm-12 ">
    <img class="rounded mx-auto d-block" src="https://mindbodygreen-res.cloudinary.com/images/w_767,q_auto:eco,f_auto,fl_lossy/usr/RetocQT/sarah-fielding.jpg" width="100" height="100">
    <h4 class="w-100 text-center mt-2">{{Auth::user()->name . '   ' .  Auth::user()->surname }}</h4>
    <ul class="nav nav-tabs p-4" role="tablist">
        <li class="nav-item w-100">
            <a @if (\Route::current()->getName() === 'user.account') class="nav-link active" @else class="nav-link" @endif href="{{route('user.account')}}">Profile</a>
        </li>
        <li class="nav-item w-100">
            <a @if (\Route::current()->getName() === 'user.bookings') class="nav-link active" @else class="nav-link" @endif href="{{route('user.bookings')}}">Bookings</a>
        </li>
        <li class="nav-item w-100">
            <a @if (\Route::current()->getName() === 'user.changePassword') class="nav-link active" @else class="nav-link" @endif href="{{route('user.changePassword')}}">Password</a>
        </li>
    </ul>
</div>