<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit</title>
</head>

<body>

    <form action="{{route('user.update',Auth::user())}}" method="POST">
        @csrf
        @method('PATCH')

        <input type="text" name="name" value="{{Auth::user()->name}}">
        <input type="text" name="surname" value="{{Auth::user()->surname}}">
        <input type="password" name="password" value="">

        <button type="submit">Save</button>
    </form>


</body>

</html>