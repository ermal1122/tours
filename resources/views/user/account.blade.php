@extends('layouts.client')

@section('content')


<div class="row mt-4 container mx-auto">
  @include('user.sidebar_tabs')
  <div class="col-lg-7 col-sm12 p-4 bg-light">
    @include('notification')
    <div class="tab-content">
      <div id="home" class="tab-pane active"><br>
        <h3>Profile</h3>
        <form action="{{route('user.update',Auth::user())}}" method="POST">
          @csrf
          @method('PATCH')
          <div class="form-group">
            <label for="fname">First Name:</label>
            <input type="text" class="form-control" name="name" value="{{Auth::user()->name}}" id="fname">
          </div>
          <div class="form-group">
            <label for="lname">Last Name:</label>
            <input type="text" class="form-control" name="surname" value="{{Auth::user()->surname}}" id="lname">
          </div>
          <div class="form-group">
            <label for="country">Phone:</label>
            <input id="phone" type="text" name="phone" placeholder="123213123" value="{{Auth::user()->phone}}" class="form-control">
          </div>
          <div class="form-group">
            <label for="country">Country:</label>
            <input type="text" class="form-control" placeholder="Alaska" v id="country">
          </div>
          <div class="form-group">
            <label for="city">City:</label>
            <input type="text" class="form-control" placeholder="Tirana" id="city">
          </div>
          <div class="form-group">
            <label for="staddress">Street Address:</label>
            <input type="text" class="form-control" placeholder="Ali Demi" id="staddress">
          </div>
          <div class="form-group">
            <label for="zipcode">Zip Code:</label>
            <input type="text" class="form-control" placeholder="10001" id="zipcode">
          </div>
          <button type="submit" class="btn btn-primary float-right">Save Changes</button>
        </form>
      </div>


    </div>
  </div>
</div>
</div>
@endsection