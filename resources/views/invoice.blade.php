<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="x-apple-disable-message-reformatting" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="color-scheme" content="light dark" />
    <meta name="supported-color-schemes" content="light dark" />
    <title>Invoice from superalbania.com</title>
    <style type="text/css" rel="stylesheet" media="all">
        /* Base ------------------------------ */
    </style>
    <!--[if mso]>
    <style type="text/css">
      .f-fallback
      
      {
        font-family: Arial, sans-serif;
      }
    </style>
  <![endif]-->
</head>

<body>
    <span class="preheader">This is an invoice for your purchase on purchase_date . Please submit payment </span>
    <table class="email-wrapper" width="100%" cellpadding="0" cellspacing="0" role="presentation">
        <tr>
            <td align="center">
                <table class="email-content" width="100%" cellpadding="0" cellspacing="0" role="presentation">
                    <tr>
                        <td class="email-masthead">
                            <a href="https://superalbania.com" class="f-fallback email-masthead_name">
                                <span class="white-logo">super<span class="blue-logo">albania.com</span></span>
                            </a>
                        </td>
                    </tr>
                    <!-- Email Body -->
                    <tr>
                        <td class="email-body" width="100%" cellpadding="0" cellspacing="0">
                            <table class="email-body_inner" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
                                <!-- Body content -->
                                <tr>
                                    <td class="content-cell">
                                        <div class="f-fallback">
                                            <h1>Hi {{$payment->name}} {{$payment->surname}}</h1>
                                            <p>Thank you for purchasing your next trip to Albania.</p>
                                            <table class="purchase" width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <h3>ID:{{$payment->payment_id}}</h3>
                                                    </td>
                                                    <td>
                                                        <h3 class="align-right">08/11/2020</h3>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table class="purchase_content" width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <th class="purchase_heading" align="left">
                                                                    <p class="f-fallback">Description</p>
                                                                </th>
                                                                <th class="purchase_heading" align="right">
                                                                    <p class="f-fallback">Amount</p>
                                                                </th>
                                                            </tr>

                                                            <tr>
                                                                <td width="80%" class=""><span class="f-fallback">Persons x {{$payment->seats}}</span></td>
                                                                <td class="align-right" width="20%" class="purchase_item"><span class="f-fallback">{{$payment->price / $payment->seats}}$</span></td>
                                                            </tr>

                                                            <tr>
                                                                <td width="80%" class="purchase_footer" valign="middle">
                                                                    <p class="f-fallback purchase_total purchase_total--label">Total</p>
                                                                </td>
                                                                <td width="20%" class="purchase_footer" valign="middle">
                                                                    <p class="f-fallback purchase_total">{{$payment->price}}$</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <p>If you have any questions about this invoice, simply reply to this email or reach out to our <a href="">support team</a> for help.</p>
                                            <p>Cheers,
                                                <br>superalbania.com</p>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="email-footer" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
                                <tr>
                                    <td class="content-cell" align="center">
                                        <p class="f-fallback sub align-center"><span class="white-logo">super<span class="blue-logo">albania.com</span></span><br> &copy; 2020 . All rights reserved.</p>
                                        <p class="f-fallback sub align-center">
                                            SuperAlbania
                                            <br>Tirana
                                            <br>Albania
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>

</html>