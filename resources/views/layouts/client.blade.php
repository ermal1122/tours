<!DOCTYPE html>
<html lang="en">

<head>

    <!-- head -->
    <meta charset="utf-8">
    <meta name="msapplication-tap-highlight" content="no" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Personalized tours in Albania">
    <meta name="author" content="Superalbania">
    <title>SuperAlbania</title>
    <!-- Stylesheets -->
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,400italic,300italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{asset('assets/css/docs.theme.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/custom.css')}}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <!--navbar links and scripts -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">


    <!-- Owl Stylesheets -->
    <link rel="stylesheet" href="{{asset('assets/owlcarousel/assets/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/owlcarousel/assets/owl.theme.default.min.css')}}">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- Favicons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="shortcut icon" href="assets/ico/favicon.png">
    <link rel="shortcut icon" href="favicon.ico">

    <!-- Yeah i know js should not be in header. Its required for demos.-->

    <!-- javascript -->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="{{asset('assets/vendors/jquery.min.js')}}"></script>
    <script src="{{asset('assets/owlcarousel/owl.carousel.js')}}"></script>

</head>

<body>
    <div class="icon-bar">
        <a href="#" class="facebook icon-bar-display"><i class="fa fa-facebook"></i></a>
        <a href="#" class="instagram icon-bar-display"><i class="fa fa-instagram"></i></a>
        <a href="#" class="whatsapp"><i class="fa fa-whatsapp"></i></a>

    </div>
    <nav class="navbar navbar-expand-lg main-navbar">

        <a class="navbar-brand" href="{{route('home.index')}}">super<span style="color: black;">albania.com</span></a>
        <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Offers</a>
                </li>
                <!-- <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Dropdown
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </li> -->
                <li class="nav-item">
                    <a class="nav-link disabled" href="#">Disabled</a>
                </li>
            </ul>
            @if(Auth::user())

            <div class="navbar-nav">
                <div class="dropdown">
                    <button class=" btn btn-default text-white" data-toggle="dropdown"><i class="material-icons profile-icon-custom align-middle">account_circle</i> <span class="align-middle">{{Auth::user()->name}}</span></button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="#">Favorites</a>
                        <a class="dropdown-item" href="{{route('user.bookings')}}">Purchases</a>
                        <a class="dropdown-item" href="{{route('user.account')}}">Account</a>
                        <form method="POST" class="logout" action="{{ route('logout') }}">
                            @csrf

                            <x-jet-dropdown-link href="{{ route('logout') }}" onclick="event.preventDefault();
                                                    this.closest('form').submit();">
                                {{ __('Logout') }}
                            </x-jet-dropdown-link>
                        </form>
                    </div>
                </div>
            </div>
            @else
            <li><a href="{{route('login')}}">Login</a></li>
            @endif
        </div>
    </nav>
    @yield('content')

    <div class="row">
        <footer class="footer-section">
            <div class="container">

                <div class="footer-content pt-5 pb-5">
                    <div class="row">
                        <div class="col-xl-4 col-lg-4 mb-50">
                            <div class="footer-widget">
                                <div class="footer-logo">
                                    <p class="footer-logo">
                                        super<span class="blue">albania.com</span>
                                    </p>
                                </div>
                                <div class="footer-text">
                                    <p>The latest offers and tours from a must explore hidden pearl of Europe, Albania.</p>
                                </div>
                                <div class="footer-social-icon">
                                    <span>Follow us</span>
                                    <a href="#"><i class="fa fa-facebook-f facebook-bg"></i></a>
                                    <a href="#"><i class="fa fa-instagram twitter-bg"></i></a>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-4 col-lg-4 col-md-6 mb-50">
                            <div class="footer-widget">
                                <div class="footer-widget-heading">
                                    <h3>Subscribe</h3>
                                </div>
                                <div class="footer-text mb-25">
                                    <p>Don’t miss an offer. Subscribe Now!</p>
                                </div>
                                <div class="subscribe-form">
                                    <form action="#">
                                        <input type="text" placeholder="Email">
                                        <button><i class="fa fa-telegram"></i></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-6 mb-30">
                            <div class="footer-widget">
                                <div class="footer-widget-heading">
                                    <h3>Payment Methods</h3>
                                    <div class="pay-meth">
                                        <div class="row">
                                            <div class="col-12">
                                                <p class="text-white mb-0"><i class="fa fa-lock text-success mr-2 lock-icon-custom"></i>Fast & Secure Payments</p>
                                            </div>
                                            <div class="col-12 mt-4">
                                                <img src="{{asset('assets/img/icons8-visa.svg')}}">
                                                <img src="{{asset('assets/img/icons8-mastercard.svg')}}">
                                                <img src="{{asset('assets/img/icons8-discover.svg')}}">
                                                <img src="{{asset('assets/img/icons8-american-express.svg')}}">
                                                <img src="{{asset('assets/img/icons8-apple-pay.svg')}}" style="filter: brightness(0) invert(1);">
                                                <img src="{{asset('assets/img/icons8-stripe.svg')}}">
                                                <i class="fa  fa-cc-paypal" style="color: #2d4794"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copyright-area">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 text-center text-lg-left">
                            <div class="copyright-text">
                                <p>Copyright &copy; 2020, All Right Reserved <a href="https://superalbania.com">superalbania.com</a></p>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 d-none d-lg-block text-right">
                            <div class="footer-menu">
                                <ul>
                                    <li><a href="#">Home</a></li>
                                    <li><a href="#">Terms</a></li>
                                    <li><a href="#">Privacy Policy</a></li>
                                    <li><a href="#">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</body>

<!--navbar scripts -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<script src="{{asset('assets/owlcarousel/owl.carousel.js')}}"></script>
<script src="{{asset('assets/js/custom.js')}}"></script>

<script src="{{asset('assets/vendors/highlight.js')}}"></script>
<script src="{{asset('assets/js/app.js')}}"></script>


<script>
    $('.sv-slider .owl-carousel').owlCarousel({
        autoplay: false,
        autoplayHoverPause: true,
        dots: false,
        nav: true,
        thumbs: true,
        thumbImage: true,
        thumbsPrerendered: true,
        thumbContainerClass: 'owl-thumbs',
        thumbItemClass: 'owl-thumb-item',
        loop: true,
        navText: [
            "<i class='fa fa-chevron-left' aria-hidden='true'></i>",
            "<i class='fa fa-chevron-right' aria-hidden='true'></i>"
        ],
        items: 1,
        responsive: {
            0: {
                items: 1,
            },
            768: {
                items: 1,
            },
            992: {
                items: 1,
            }
        }
    });
</script>



</body>

</html>
