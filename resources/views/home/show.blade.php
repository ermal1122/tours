@extends('layouts.client')

@section('content')

@section('content')
<div class="container-fluid p-5">
  <div class="row mw-100">
    <div class="col-lg-8">
      <div class="row mb-2 mr-1">

        <div id="custCarousel" class="carousel slide w-100" data-ride="carousel">
          <!-- slides -->
          <div class="carousel-inner w-100">
            <div class="carousel-item active w-100"> <img src="http://127.0.0.1:8000/upload/posts/1602191114.jpg" alt="Hills"> </div>
            <div class="carousel-item w-100"> <img src="http://127.0.0.1:8000/upload/posts/1602191114.jpg" alt="Hills"> </div>
            <div class="carousel-item w-100"> <img src="http://127.0.0.1:8000/upload/posts/1602191114.jpg" alt="Hills"> </div>
            <div class="carousel-item w-100"> <img src="http://127.0.0.1:8000/upload/posts/1602191114.jpg" alt="Hills"> </div>
            <div class="carousel-item w-100"> <img src="http://127.0.0.1:8000/upload/posts/1602191114.jpg" alt="Hills"> </div>
            <div class="carousel-item w-100"> <img src="http://127.0.0.1:8000/upload/posts/1602191114.jpg" alt="Hills"> </div>
            <div class="carousel-item w-100"> <img src="http://127.0.0.1:8000/upload/posts/1602191114.jpg" alt="Hills"> </div>
          </div> <!-- Left right --> <a class="carousel-control-prev" href="#custCarousel" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a> <a class="carousel-control-next" href="#custCarousel" data-slide="next"> <span class="carousel-control-next-icon"></span> </a> <!-- Thumbnails -->
          <ol class="carousel-indicators list-inline mt-n2 mb-5">
            <li class="list-inline-item active"> <a id="carousel-selector-0" class="selected" data-slide-to="0" data-target="#custCarousel"> <img src="http://127.0.0.1:8000/upload/posts/1602191114.jpg" class="img-fluid"> </a> </li>
            <li class="list-inline-item"> <a id="carousel-selector-1" data-slide-to="1" data-target="#custCarousel"> <img src="http://127.0.0.1:8000/upload/posts/1602191114.jpg" class="img-fluid"> </a> </li>
            <li class="list-inline-item"> <a id="carousel-selector-2" data-slide-to="2" data-target="#custCarousel"> <img src="http://127.0.0.1:8000/upload/posts/1602191114.jpg" class="img-fluid"> </a> </li>
            <li class="list-inline-item"> <a id="carousel-selector-3" data-slide-to="3" data-target="#custCarousel"> <img src="http://127.0.0.1:8000/upload/posts/1602191114.jpg" class="img-fluid"> </a> </li>
            <li class="list-inline-item"> <a id="carousel-selector-4" data-slide-to="3" data-target="#custCarousel"> <img src="http://127.0.0.1:8000/upload/posts/1602191114.jpg" class="img-fluid"> </a> </li>
            <li class="list-inline-item"> <a id="carousel-selector-5" data-slide-to="3" data-target="#custCarousel"> <img src="http://127.0.0.1:8000/upload/posts/1602191114.jpg" class="img-fluid"> </a> </li>
            <li class="list-inline-item"> <a id="carousel-selector-6" data-slide-to="3" data-target="#custCarousel"> <img src="http://127.0.0.1:8000/upload/posts/1602191114.jpg" class="img-fluid"> </a> </li>
            <li class="list-inline-item"> <a id="carousel-selector-5" data-slide-to="3" data-target="#custCarousel"> <img src="http://127.0.0.1:8000/upload/posts/1602191114.jpg" class="img-fluid"> </a> </li>
          </ol>
        </div>

      </div>
      @if(!empty($post->days_json))
      <div class="row p-4 mb-2 mr-1 rounded-lg bg-light">
        <h5 class="font-weight-bolder ml-n2 mb-2 w-100">Itenerary</h5>
        <ul class="iten-days">
          <?php $i = 1 ?>
          @foreach($post->days as $day)

          <li class="ml-4 font-weight-bold">Day {{$i}}</li>
          <p class="itenerary-p-custom">{{$day}}</p>
          <?php $i++; ?>
          @endforeach
        </ul>
      </div>
      @endif

      <div class="row p-4 mb-2 mr-1 rounded-lg bg-light">
        <h5 class="font-weight-bolder ml-n2 mb-2">Map</h5>
      </div>
      <div class="row p-4 mb-2 mr-1 rounded-lg bg-super">
        <h5 class="ml-auto mr-auto font-weight-bold w-100">Wanna find a good flight or hotel for the duration of your stay?</h5>
        <h5 class="ml-auto mr-auto font-weight-bold">Let us help!</h5>
      </div>
      <div class="row p-4 mb-2 mr-1 rounded-lg bg-light">
        <h5 class="font-weight-bolder ml-n2 mb-2">Reviews</h5>
      </div>

    </div>
    <div class="col-lg-4">
      <div class="row p-4 mb-2 rounded-lg bg-light">
        <h4>{{$post->title}}</h4>
        <p>{{$post->description}}</p>
        <h5>Available Dates</h5>


        <select class="date-aviable" name="dates" id="date-avaiable">
          @if(count($post->dates)>0)
          @foreach($post->dates as $date)
          <option data-start="{{$date->start_format_hour}}" data-end="{{$date->end_format_hour}}" value="{{$date->start_format_hour}}">{{$date->start_format}}</option>
          @endforeach
          @endif
        </select>

      </div>
      <div class="row p-4 mb-2 rounded-lg bg-light">
        <span class="align-middle mt-1 price-custom">${{$post->price}}</span><button class="btn btn-secondary ml-auto bg-dark align-middle">Book Now</button><a href="{{Auth::user() ? route('home.checkout',[$post->id,$post->title]) : route('login')}}" style="text-decoration : none"><button>
      </div>
      <div class="row p-4 mb-2 rounded-lg bg-light">
        <h5 class="font-weight-bolder ml-n2 mb-2">Details</h5>
        <div class="row w-100 mb-1">
          <div class="col-1">
            <span class="fa fa-clock-o"></span>
          </div>
          <div class="col-auto">
            <span class="font-weight-bold ml-n2">Duration:</span><span> {{$post->duration}} days</span>
          </div>
        </div>
        <div class="row w-100 mb-1">
          <div class="col-1">
            <span class="fa fa-hotel"></span>
          </div>
          <div class="col-auto">
            <span class="font-weight-bold ml-n2">Accomodation:</span><span> {{$post->Accomodation}} </span>
          </div>
        </div>
        <div class="row w-100 mb-1">
          <div class="col-1">
            <span class="fa fa-ticket"></span>
          </div>
          <div class="col-auto">
            <span class="font-weight-bold ml-n2">Mobile Ticket:</span><span> {{$post->mobile_ticket == 1 ? 'Accepted' : 'Unaccepted' }}</span>
          </div>
        </div>
        <div class="row w-100 mb-1">
          <div class="col-1">
            <span class="fa fa-mail-reply"></span>
          </div>
          <div class="col-auto">
            <span class="font-weight-bold ml-n2">Cancellation:</span><span> {{$post->cancellation}} days in advance</span>
          </div>
        </div>

        <div class="row w-100 mb-1">
          <div class="col-1">
            <span class="fa fa-language"></span>
          </div>
          <div class="col-auto">
            <span class="font-weight-bold ml-n2">Languages:</span><span> {{$post->languages}}</span>
          </div>
        </div>
        <div class="row w-100 mb-1">
          <div class="col-1">
            <span class="fa fa-check-circle"></span>
          </div>
          <div class="col-auto">
            <span class="font-weight-bold ml-n2">Confirmation:</span><span> Within {{$post->confirmation}} h</span>
          </div>
        </div>

      </div>


      <div class="row p-4 mb-2 rounded-lg bg-light">
        <h5 class="font-weight-bolder ml-n2 mb-2">Important Info</h5>
        <div class="row w-100">
          <div class="col-12">
            <span class="fa fa-arrow-circle-up"></span>
            <span class="font-weight-bold">Departure Point:</span>
            <p>{{$post->departure}}</p>
          </div>
        </div>

        <div class="row w-100">
          <div class="col-12">
            <span class="fa fa-calendar-check-o"></span>
            <span class="font-weight-bold">Departure Time:</span>
            <p>September 27th, 9:00 AM</p>
          </div>
        </div>

        <div class="row w-100">
          <div class="col-12">
            <span class="fa fa-arrow-circle-down"></span>
            <span class="font-weight-bold">Return Point:</span>
            <p>{{$post->return}}</p>
          </div>
        </div>

        <div class="row w-100">
          <div class="col-12">
            <span class="fa  fa-calendar-minus-o"></span>
            <span class="font-weight-bold">Return Time:</span>
            <p>October 5th, 10:00 PM</p>
          </div>
        </div>

        <div class="row w-100">
          <div class="col-12">
            <span class="fa fa-toggle-on"></span>
            <span class="font-weight-bold">Inclusions:</span>
            <p>{{$post->inclusions}} </p>
          </div>
        </div>

        <div class="row w-100">
          <div class="col-12">
            <span class="fa fa-toggle-off"></span>
            <span class="font-weight-bold">Exclusions:</span>
            <p>Airport pickup, Hotel pickup</p>
          </div>
        </div>

      </div>
      <div class="row p-4 mb-2 rounded-lg bg-light">
        <h5 class="font-weight-bolder ml-n2 mb-2">Contact Us</h5>
      </div>
    </div>
  </div>
</div>







<script>
  $('.sv-slider .owl-carousel').owlCarousel({
    autoplay: false,
    autoplayHoverPause: true,
    dots: false,
    nav: true,
    thumbs: true,
    thumbImage: true,
    thumbsPrerendered: true,
    thumbContainerClass: 'owl-thumbs',
    thumbItemClass: 'owl-thumb-item',
    loop: true,
    navText: [
      "<i class='fa fa-chevron-left' aria-hidden='true'></i>",
      "<i class='fa fa-chevron-right' aria-hidden='true'></i>"
    ],
    items: 1,
    responsive: {
      0: {
        items: 1,
      },
      768: {
        items: 1,
      },
      992: {
        items: 1,
      }
    }
  });


  $('.date-aviable').change(function() {
    startDate = $(this).find(':selected').data('start');
    endDate = $(this).find(':selected').data('end');

    $('.first_start').replaceWith("<p class='first_start'>" + startDate + "</p>");
    $('.first_end').replaceWith("<p class='first_end'>" + endDate + "</p>");
  })
</script>


@endsection