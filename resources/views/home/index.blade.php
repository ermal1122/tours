@extends('layouts.client')

@section('content')


<div class="container-fluid p-0">
    <div class="jumbotron text-white w-100 rounded-0 bg-jumbotron-custom">

        <h1 class="text-white">Karaburun</h1>
        <br>
        <p class="text-white">Visit the hidden pearl and go on a trip to discover yourself!<br><br>
            <i class="material-icons mr-2 text-white">flight_takeoff</i><span class="mr-3 mb-3 text-white">Flight Included</span>
            <i class="material-icons mr-2 text-white">account_balance_wallet</i><span class="mr-3 mb-3 text-white">Flight Included</span>
            <i class="material-icons mr-2 text-white">watch_later</i><span>6 days</span>
        </p>
    </div>


    <!--Tours Carousel-->
    <h3 class="mb-n5 pb-1 pl-2">Daily Tours</h3>
    <!-- body -->
    <div class="home-demo w-100 float-right">
        <div class="row mw-100 float-right">
            <div class="col-lg-12  float-right">
                <div class="owl-carousel  float-right">

                    @foreach($posts as $post)
                    <div class="item mw-25 bg-light ">
                        <i class="material-icons float-right save-icon-custom favorite" data-toggle="tooltip" data-placement="left" title="Add to favorites">favorite_border</i>
                        <a href="{{route('home.show',$post->id)}}">
                            <div class="row">
                                <div class="col-lg-12">
                                    <img class="img-carousel" src="{{asset('upload/posts') . '/' . $post->profile_photo}}">
                                </div>
                            </div>
                            <div class="row pt-2 mb-n1">
                                <div class="col-lg-12 pl-4">
                                    <h5 class="item-title-custom">{{$post->title}}</h5>
                                </div>
                            </div>
                            <div class="row reviews-custom">
                                <div class="col col-sm-6 text-super pl-4 mb-0 reviews-custom">
                                    <i class="material-icons star-icons-custom ">star</i>
                                    <i class="material-icons star-icons-custom">star</i>
                                    <i class="material-icons star-icons-custom">star</i>
                                    <i class="material-icons star-icons-custom">star</i>
                                    <i class="material-icons star-icons-custom">star_border</i>
                                </div>
                                <div class="col pr-4 text-right mb-0 extra-info-custom">
                                    <span class="text-secondary mt-n2 ">3 cities {{$post->duration}}</span>
                                </div>
                            </div>

                            <div class="row pb-2">
                                <div class="col-8 cat-custom pl-4 mt-1">
                                    <span class="bg-cat-custom rounded px-2 py-1">Child-friendly</span>
                                    <span class="bg-cat-custom rounded px-2 py-1">Culinary</span>
                                </div>
                                <div class="col-4 text-right pr-4">
                                    {{$post->price}} $
                                </div>
                            </div>
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <div class="row main-helper-custom mw-100 mb-2 p-2">
        <div class="col-lg-5 bg-super d-flex ">
            <div class="row">
                <div class="col-12 my-auto">
                    <span class="text-white my-auto mb-4 question-contact-custom">Need help planning your <b><i>next trip?</i></b></span><br>
                    <button class="btn text-white bg-dark rounded-0 button-index-contact-custom">Contact Us</button>
                </div>
            </div>

        </div>
        <div class="col-lg-7 bg-image-custom">
        </div>
    </div>


</div>


<!--Scripts-->
<script>
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });

    $('.favorite').click(function() {
        if ($(this).text() == 'favorite_border') {
            $(this).text('favorite');
        } else
            $(this).text('favorite_border');
    })
</script>
<script>
    var owl = $('.owl-carousel');
    owl.owlCarousel({
        loop: false,
        freeDrag: false,
        nav: true,
        singleItem: true,
        dots: true,
        items: 1,
        touchDrag: true,
        merge: false,
        slideBy: 1,
        margin: 20,
        responsive: {
            0: {
                items: 1.3,
                margin: 10,
                center: true,
            },
            600: {
                items: 2,
                margin: 10,
                slideBy: 1,
            },
            800: {
                items: 3,
                margin: 10,
                slideBy: 3,
            },
            1000: {
                items: 4,
                margin: 20,
                slideBy: 3,

            }
        }
    });
</script>
<script>
    $(".save-offer").click(function() {

        if ($(".save-offer").hasClass("fa-heart-o")) {
            $(this).toggleClass('fa-heart-o');
            $(this).toggleClass('fa-heart');
            $(".saved-pop-time").show("moderate").delay(700).hide("slow");
            $(this).preventDefault();
        } else {
            $(".unsaved-pop-time").show("moderate").delay(700).hide("slow");
            $(this).toggleClass('fa-heart');
            $(this).toggleClass('fa-heart-o');
            $(this).preventDefault();
        }
    });
</script>
<script>
    $(".save-offer").click(function() {

        if ($(".save-offer").hasClass("fa-heart-o")) {}
    });
</script>
<script src="assets/vendors/highlight.js"></script>
<script src="assets/js/app.js"></script>
</body>

@endsection