@extends('layouts.client')

@section('content')

<div class="container payment-container p-4">
    @if(Session::has('error'))
    <div class="alert alert-danger">
        {{ Session::get('error')}}
    </div>
    @endif

    <form method="POST" action="{{route('paypal.pay')}}">
        @csrf
        <div class="row-payment row">
            <div class="col-8 billing-custom">
                @if ($errors->any())
                @foreach ($errors->all() as $error)
                <div class="alert-danger">{{$error}}</div>
                @endforeach
                @endif
                <h3>Billing Information</h3>
                <label for="fname"><i class="fa fa-user"></i> Full Name</label>
                <input required type="text" id="fname" name="name" placeholder="John M. Doe">
                <label for="email"><i class="fa fa-envelope"></i> Email</label>
                <input required type="email" id="email" name="email" placeholder="john@example.com">
                <label for="adr"><i class="fa fa-address-card-o"></i> Address</label>
                <input required type="text" id="adr" name="address" placeholder="542 W. 15th Street">
                <label for="city"><i class="fa fa-institution"></i> City</label>
                <input required type="text" id="city" name="city" placeholder="New York">
                <label for="state">State</label>
                <input required type="text" id="state" name="country" placeholder="NY">
                <label for="zip">Zip</label>
                <input required type="text" id="zip" name="postal_code" placeholder="10001">
                <label>Number of People</label>
                <select class="col-5" id="people_number" style="height:40px;" name="people_nr">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                </select>
            </div>

            <div class="col-4 checkout-custom">

                <h3 class="text-right">Cart <span class="total-nr" style="color:black">
                        <!-- <i class="fa fa-shopping-cart"></i>4</span> -->
                </h3>
                <div class="row rounded bg-white p-2 mt-2">
                    <div class="col-lg-10 col-sm-10">
                    <div class="row w-100">
                        {{-- <div class="col-4">
                          <img class="rounded justify-content-center align-self-center align-middle" src="https://www.state.gov/wp-content/uploads/2018/11/Albania-2218x1406.jpg">
                        </div> --}}
                        <div class="col-10 ">
                          <span class="" style="font-size: 13px; font-weight:700;line-height: 1.2;
                          display: block;">Mountain Adveture in Southeast Albania with professional guide</span>
                          <span class="d-block" style="font-size: 11px;">6 days</span>
                      </div>
                    <div class="col-lg-2 col-sm-2 justify-content-center align-self-center">
                      <span class="">$76.18</span>
                    </div>
                  </div>
                </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-6">
                        <h3>Total</h3>
                    </div>
                    <div class="col-6">
                        <span class="float-right total-nr text-right justify-content-center align-self-center" id="total-price" data-price="{{$post->price}}">${{$post->price}}</span>
                    </div>
                </div>
                <input type="hidden" name="post_id" value="{{$post->id}}">
                <br>
                <input type="submit" value="Continue to checkout" class="btn-payment">
            </div>

        </div>
        


    </form>
</div>


<script>
    $('#people_number').change(function() {
        let number_people = $(this).val();
        let price = $('#total-price').data('price');
        $(".total-nr>b").html('$' + price * number_people);
        $('#nr_people').val(number_people * price)

    });
</script>

@endsection