@extends('layouts.admin')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">General Form</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        @if (session()->has('success'))
        <div class="alert alert-success">
            <h1>{{ session('success') }}</h1>
        </div>
        @endif
        @if (session()->has('failed'))
        <div class="alert alert-success">
            <h1>{{ session('failed') }}</h1>
        </div>
        @endif
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Create Post</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" method="POST" action="{{route('post.update', $post->id)}}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            @method('PUT')
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Title</label>
                                    <input type="text" value="{{$post->title}}" name="title" class="form-control @error('title') is-invalid @enderror" placeholder="Title">
                                    @error('title')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="">Description</label>
                                    <textarea class="form-control @error('title') is-invalid @enderror" name="description" rows="4" cols="50">{{$post->description}} </textarea>
                                    @error('description')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <label for="">Duration</label>
                                        <input type="text" name="duration" value="{{$post->duration}}" class="form-control" placeholder="Duration">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="">Price</label>
                                        <input type="number" name="price" value="{{$post->price}}" class="form-control @error('title') is-invalid @enderror" placeholder="Price">
                                        @error('price')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="">Departure</label>
                                        <input type="text" value="{{$post->departure}}" name="departure" class="form-control" placeholder="Departure">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="">Return</label>
                                        <input type="text" name="return" value="{{$post->return}}" class="form-control" placeholder="Return">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="">City</label>
                                        <input type="text" name="city" value="{{$post->city}}" class="form-control" placeholder="City">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="">Free Seats</label>
                                        <input type="text" name="free_seats" value="{{$post->free_seats}}" class="form-control" placeholder="Seats">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="">Category</label>
                                        <select class="form-control" name="category_id">
                                            @foreach($categories as $category)
                                            <option value="{{$category->id}}" {{$category->id == $post->category_id ? 'selected' : ''}}>{{$category->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-md-6 form-group">
                                        <label for="">Acomodation</label>
                                        <input type="text" value="{{$post->acomodation}}" name="acomodation" class="form-control" placeholder="Acomodation">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="">Languages</label>
                                        <input type="text" value="{{$post->languages}}" name="languages" class="form-control" placeholder="Languages">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="">Inclusions</label>
                                        <input type="text" name="inclusions" value="{{$post->inclusions}}" class="form-control" placeholder="Inclusions">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="">Confirmation</label>
                                        <input type="number" name="confirmation" value="{{$post->confirmation}}" class="form-control" placeholder="Confirmation">
                                    </div>

                                    <div class="col-md-6 form-group">
                                        <label for="">Cancellation</label>
                                        <input type="number" name="cancellation" value="{{$post->cancellation}}" class="form-control" placeholder="Cancellation">
                                    </div>
                                    <div class="col-md-6 form-group mt-3">
                                        <br>
                                        <label for=""> Mobile Ticket</label>
                                        <input class="ml-3 " type="checkbox" value="1" name="mobile_ticket" placeholder="Moobile Ticket" {{$post->mobile_ticket == 1 ? 'checked' : ''}}>
                                    </div>

                                    @if(!empty($post->dates))
                                    @foreach($post->dates as $dates)

                                    <div id="inputFormRow" class="form-group col-md-12 ">
                                        <label> Selected Date range:</label>
                                        <div class="input-group ">
                                            <div class="input-group-prepend ">
                                                <span class="input-group-text">
                                                    <i class="far fa-calendar-alt"></i>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control float-right date-picker" name="dates[]" value="{{$dates->start}} - {{$dates->end}}" data-id="{{$dates->id}}">
                                            <div class="input-group-append ">
                                                <button id="removeDateRow" type="button" class="btn btn-danger">Remove</button>
                                            </div>
                                        </div>
                                        <!-- /.input group -->

                                    </div>

                                    @endforeach
                                    @endif

                                    <div id="newRowDate" class="form-group  col-md-12">
                                    </div>
                                    <a id="addRowDate" class="btn btn-info mt-2 mb-4">Add Date</a>

                                </div>

                                @if(!empty($post->days_json))
                                <?php $i = 1 ?>
                                @foreach($post->days as $day)
                                <div class="form-group">
                                    <div id="inputFormRow">
                                        <label for="">Day {{$i}}</label>
                                        <div class="input-group mb-3">
                                            <input type="text" name="days_json[]" class="form-control m-input" placeholder="Enter title" value="{{$day}}" autocomplete="off">
                                            <div class="input-group-append">
                                                <button id="removeRow" type="button" class="btn btn-danger">Remove</button>
                                            </div>
                                        </div>
                                    </div>
                                    <?php $i++ ?>
                                    @endforeach
                                    @endif

                                    <div id="newRow"></div>
                                    <a id="addRow" class="btn btn-info">Add Row</a>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputFile">File input</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" id="imgInp" name="profile_photo" class="custom-file-input">
                                            <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                        </div>
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="">Upload</span>
                                        </div>
                                    </div>
                                    <img class="admin-post-photos mb-2 mt-5" id="hide-photo" src="{{asset('upload/posts') . '/' . $post->profile_photo}}" height="500px;">
                                    <img id="preview-photo" class="admin-post-photos mt-5" src="#" alt="your image" height="500px;" />
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputFile">Multiple Photos </label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" multiple id="gallery-photo-add" name="multiple_photos[]" class="custom-file-input">
                                            <label class="custom-file-label">Choose files</label>
                                        </div>
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="">Uploads</span>
                                        </div>

                                    </div>

                                </div>


                            </div>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                    </form>
                    @if($post->photos->count() > 0)
                    <div class="row mt-5">
                        @foreach($post->photos as $photo)
                        <form action="{{route('post.galleryDelte',$photo->id)}}" method="POST" class="col-md-3 ">
                            @method('DELETE')
                            @csrf
                            <img class="admin-post-photos mb-2" src="{{asset('upload/gallery') . '/' . $photo->name}}">
                            <!-- <button type="submit"><i class="far fa-times-circle float-right"></i></button> -->
                            <i onclick="$(this).closest('form').submit();" class="far fa-times-circle float-right"></i>
                        </form>
                        @endforeach
                    </div>
                    @endif

                </div>
            </div>
        </div>
        <!-- /.row -->
</div><!-- /.container-fluid -->
</section>
<!-- /.content -->
<div class="row gallery ml-2"></div>
</div>
<script>
    src = "https://code.jquery.com/jquery-3.5.1.min.js"
    integrity = "sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
    crossorigin = "anonymous" >
</script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>


<script>
    $(function() {
        $('input[name="daterange"]').daterangepicker({
            opens: 'left'
        }, function(start, end, label) {
            console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
        });
    });
    $(".date-picker").click(function() {
        let id = $(this).data("id");
        alert(id)
        $(this).daterangepicker({
            timePicker: true,
            timePickerIncrement: 30,

            locale: {
                format: 'MM/DD/YYYY hh:mm A'
            }
        });

    });
</script>
<script>
    // add row
    $("#addRow").click(function() {
        var html = '';
        html += '<div id="inputFormRow">';
        html += '<div class="input-group mb-3">';
        html += '<input name="days_json[]" rows="4" cols="50" class="form-control m-input" placeholder="Enter title" autocomplete="off">';
        html += '<div class="input-group-append">';
        html += '<a id="removeRow" class="btn btn-danger">Remove</a>';
        html += '</div>';
        html += '</div>';

        $('#newRow').append(html);
    });

    // remove row
    $(document).on('click', '#removeRow', function() {
        $(this).closest('#inputFormRow').remove();
    });

    let id = 0;
    // add row date
    $("#addRowDate").click(function() {
        // let id=$('#reservation').data("id");
        id++;
        var html = '';
        html += '<div id="inputFormRow" class="input-group mt-4">';
        html += '<div class="input-group-prepend">';
        html += '<span class="input-group-text"><i class="far fa-calendar-alt"></i></span>'
        html += '</div>';
        html += '<input type="text" name="dates[]" class="form-control float-right" id="reservation-' + id + '">'
        html += '<a id="removeDateRow" class="btn btn-danger">Remove</a>';
        html += '</div>';


        $('#newRowDate').append(html);
        $('#reservation-' + id + '').daterangepicker({
            timePicker: true,
            timePickerIncrement: 30,
            locale: {
                format: 'MM/DD/YYYY hh:mm A'
            }
        })
    });

    // remove row
    $(document).on('click', '#removeDateRow', function() {
        $(this).closest('#inputFormRow').remove();
    });







    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#preview-photo').show();
                $('#hide-photo').hide();
                $('#preview-photo').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]); // convert to base64 string
        }
    }

    $("#imgInp").change(function() {
        readURL(this);
    });




    $(function() {
        // Multiple images preview in browser
        var imagesPreview = function(input, placeToInsertImagePreview) {
            let images = '';
            if (input.files) {
                var filesAmount = input.files.length;
                $('.refresh').remove();

                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();
                    reader.onload = function(event) {
                        images = $(".gallery").append('<div class="col-md-3 refresh"><img class="admin-post-photos float-left" src="' + event.target.result + '"></div> ');
                    }

                    reader.readAsDataURL(input.files[i]);
                }
            }

        };

        $('#gallery-photo-add').on('change', function() {
            imagesPreview(this, 'div.gallery');
        });
    });
</script>

@endsection