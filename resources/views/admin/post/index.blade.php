@extends('layouts.admin')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>DataTables</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">DataTables</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">DataTable with minimal features & hover style</h3>
            </div>
            <div class="col-md-6">
              <a href="{{route('post.create')}}" class="btn btn-primary mt-3 ml-3">Create Post</a>
            </div>

            <!-- /.card-header -->
            <div class="card-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Price</th>
                    <th>Days</th>
                    <th>Duration</th>
                    <th>Departure</th>
                    <th>Return</th>
                    <th>City</th>
                    <th>View</th>
                    <th>Delete</th>



                  </tr>
                </thead>
                <tbody>
                  @foreach($posts as $post)
                  <tr>
                    <td>{{$post->title}}</td>
                    <td>{{$post->description}}</td>
                    <td>{{$post->price}}</td>
                    <td>{{$post->days_json}}</td>
                    <td>{{$post->duration}}</td>
                    <td>{{$post->departure}}</td>
                    <td>{{$post->return}}</td>
                    <td>{{$post->city}}</td>
                    <td><a href="{{route('admin.show',$post->id)}}">View</a></td>
                    <td>
                      @if($post->deleted_at == null)
                      <form method="POST" action="{{route('post.softDelete',$post->id)}}">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-sm btn-danger" type="submit">Delete</button>
                      </form>
                      @else
                      <form method="POST" action="{{route('post.restore')}}">
                        @csrf
                        @method('PATCH')
                        <input hidden value="{{$post->id}}" name="post_id" type="text">
                        <button class="btn btn-sm btn-success" type="submit">Restore</button>
                      </form>
                      @endif
                    </td>

                  </tr>
                  @endforeach

                  </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- page script -->
<script>
  $(function() {
    $("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>

@endsection