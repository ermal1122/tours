@if (session()->has('success'))
<div class="alert-success" id="popup_notification">
    {{ session('success') }}
</div>
@endif

@if (session()->has('error'))
<div class="alert-danger" id="popup_notification">
    {{ session('error') }}
</div>
@endif

@if($errors->any())
<div class="alert-danger" id="popup_notification">
    @foreach ($errors->all() as $error)
    <div>{{$error}}</div>
    @endforeach</div>
@endif