<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use Carbon\Carbon;

class HomeController extends Controller
{
    public function index()
    {
        $posts = Post::all();
        return view('home.index', compact('posts'));
    }

    public function show($id)
    {
        $post  = Post::with(['dates', 'photos', 'category'])->get()->find($id);
        $dates = array();
        $array_dates = array();
        $times = array();

        $array_dates_end = array();
        $dates_end = array();
        $times_end = array();

        $post->days = json_decode($post->days_json);
        foreach ($post->dates as $date) {

            $date->start_format = Carbon::parse($date->start)->format('M d Y ');
            $date->start_format_hour = Carbon::parse($date->start)->format('M d Y  h:i A');

            $date->end_format = Carbon::parse($date->end)->format('M d Y');
            $date->end_format_hour = Carbon::parse($date->end)->format('M d Y  h:i A');
        }
        return view('home.show', compact('post', 'dates'));
    }
}
