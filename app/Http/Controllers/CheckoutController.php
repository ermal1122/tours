<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use App\Models\Post;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;

class CheckoutController extends Controller
{

    public function index($id)
    {
        $post = Post::findOrFail($id);

        return view('home.checkout', compact('post'));
    }

    public function success($paymeny_id)
    {
        $payment =  Payment::where('payment_id', '=', $paymeny_id)->with('user')->firstOrFail();

        return view('checkout.done', compact('payment'));
    }

    public function generatePdf(Payment $payment)
    {

        $pdf = PDF::loadView('invoice', compact('payment'));

        return $pdf->download('invoice.pdf');
    }
}
