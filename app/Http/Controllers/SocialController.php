<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class SocialController extends Controller
{


    public function redirectToProvider(string $provider): RedirectResponse
    {
        return Socialite::driver('facebook')->redirect();
    }


    public function handleProviderCallback(string $provider): RedirectResponse
    {

        $userSocial = Socialite::driver('facebook')->user();
        // dd($userSocial);
        $findUser = User::where('email', $userSocial->email)->first();
        if ($findUser) {
            Auth::login($findUser);
            return redirect('/');
        } else {

            $name = explode(" ", $userSocial->name);
            $user = new User;
            $user->name = $name[0];
            $user->surname = $name[1];
            $user->email = $userSocial->getEmail();
            $user->password = bcrypt(123456);
            $user->save();
            Auth::login($user);
            return redirect('/');
        }
    }
}
