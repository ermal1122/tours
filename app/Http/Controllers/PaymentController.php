<?php

namespace App\Http\Controllers;

use App\Http\Requests\CheckoutRequest;
use App\Mail\PaymenytMail;
use App\Models\Post;
use App\Payment\Cashier;
use App\Models\Payment as PaymentModel;

use Illuminate\Support\Facades\Config;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;

/** All Paypal Details class **/

use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Redirect;
use Session;
use URL;

class PaymentController extends Controller
{

    public function __construct()
    {
        /** PayPal api context **/
        $paypal_conf = Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
            $paypal_conf['client_id'],
            $paypal_conf['secret']
        ));
        $this->_api_context->setConfig($paypal_conf['settings']);
    }


    public function pay(Request $request)
    {
        $address = $request->address;
        $info = [
            "sellerId" => 250535517940,
            "merchantOrderId" => rand(1000, 10000),
            "token" => $request->token,
            "currency" => 'USD',
            "total" => '10.00',
            "demo" => true,
            "billingAddr" => array(
                "name" => 'Testing Tester',
                "addrLine1" => '123 Test St',
                "city" => 'Columbus',
                "state" => 'OH',
                "zipCode" => '43123',
                "country" => 'USA',
                "email" => 'testingtester@2co.com',
                "phoneNumber" => '555-555-5555'
            ),
            "shippingAddr" => array(
                "name" => 'Testing Tester',
                "addrLine1" => '123 Test St',
                "city" => 'Columbus',
                "state" => 'OH',
                "zipCode" => '43123',
                "country" => 'USA',
                "email" => 'testingtester@2co.com',
                "phoneNumber" => '555-555-5555'
            )

        ];
        // dd($info);
        $res = Cashier::pay($info);

        if (!$res['success']) {
            return "Error";
        };

        return  "Works";
    }

    public function index()
    {
        return view('test');
    }

    public function payWithpaypal(CheckoutRequest $request)
    {

        $post = Post::findOrFail($request->post_id);
        $total_price = $post->price * $request->people_nr;

        if ($post->free_seats - $request->people_nr < 0) {
            Session::flash('error', 'No Seats Left');

            return Redirect::route('home.checkout', [$post->id, $post->title]);
        } else {
            $details = array(
                'user_id' => Auth::id(),
                'post_id' => $post->id,
                'name' => $request->name,
                'email' => $request->email,
                'address' => $request->address,
                'city' => $request->city,
                'country' => $request->country,
                'postal_code' => $request->postal_code,
                'seats' => $request->people_nr,
                'price' => $total_price,
                'completed' => true
            );
            session(['details' => $details]);
        }

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $item_1 = new Item();
        $item_1->setName($post->title)
            /** item name **/
            ->setCurrency('EUR')
            ->setQuantity($request->people_nr)
            ->setPrice($post->price);
        /** unit price **/

        $item_list = new ItemList();
        $item_list->setItems(array($item_1));

        $amount = new Amount();
        $amount->setCurrency('EUR')
            ->setTotal($total_price);
        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Your transaction description');
        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::route('status'))
            /** Specify return URL **/
            ->setCancelUrl(URL::route('status'));

        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));
        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            if (Config::get('app.debug')) {
                Session::flash('error', 'Connection timeout');
                return Redirect::route('home.index');
            } else {
                Session::flash('error', 'Some error occur, sorry for inconvenient');
                return Redirect::route('home.show');
            }
        }

        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());
        if (isset($redirect_url)) {
            /** redirect to paypal **/
            return Redirect::away($redirect_url);
        }
        Session::flash('error', 'Unknown error occurred');
        return Redirect::route('home.show');
    }

    public function getPaymentStatus()
    {
        $details = session()->get('details');
        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');
        $details['payment_id'] = $payment_id;
        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');
        if (empty(request()->PayerID) || empty(request()->PayerID)) {
            Session::flash('error', 'Payment failed');
            return Redirect::route('/');
        }
        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId(request()->PayerID);
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);
        if ($result->getState() == 'approved') {
            Session::flash('success', 'Payment success');
            try {
                $post = Post::find($details['post_id']);
                $post->update(['free_seats' => $post->free_seats - $details['seats']]);
                $final_payment = PaymentModel::create($details);
                Mail::to($details['email'])->send(new PaymenytMail($final_payment));
                return Redirect::route('home.index');
            } catch (\Throwable $th) {
                Session::flash('error', $th);
                return Redirect::route('home.checkout', [$post->id, $post->title]);
            }
        }
        Session::flash('error', 'Payment failed');
        return Redirect::route('home.checkout');
    }
}
