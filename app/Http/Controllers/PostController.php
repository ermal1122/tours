<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Post;
use App\Models\Photo;
use App\Models\Date;

use App\Http\Requests\StorePostRequest;
use App\Models\TemporaryFile;
use Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Throwable;

class PostController extends Controller
{

    public function adminIndex()
    {
        return view('admin/index');
    }

    public function index()
    {
        $posts = Post::withTrashed()->get();
        return view('admin/post.index', compact('posts'));
    }

    public function show($id)
    {
        $post = Post::findOrFail($id);
        $post->days = json_decode($post->days_json);
        $categories = Category::all();
        return view('admin.post/show', compact('post', 'categories'));
    }

    public function create()
    {
        $categories = Category::all();
        return view('admin.post/create', compact('categories'));
    }

    public function store(StorePostRequest $request)
    {

        if ($request['avatar']) {
            $temporaryFile = TemporaryFile::where('folder', $request['avatar'])->first();

            Storage::move('avatars/tmp' . $temporaryFile->folder  . '/' .   $temporaryFile->filename, public_path('test/' . $temporaryFile->filename));
            Storage::deleteDirectory('avatars/tmp' . $temporaryFile->folder);
        }

        dd("ED");

        $arrayDates = array();
        $input = $request->except('dates', 'multiple_photos');
        $input['days_json'] = json_encode($input['days_json']);
        $data = array();
        if ($request->hasFile('profile_photo')) {
            $image = $request->file('profile_photo');
            $name = time() . '.' .  $image->getClientOriginalExtension();
            $destinationPath = public_path('/upload/posts');
            $image->move($destinationPath, $name);
            $input['profile_photo'] = $name;
        }
        $post = Post::create($input);

        if ($request->hasFile('multiple_photos')) {
            $i = 1;
            foreach ($request->file('multiple_photos') as $file) {
                $name = time() .  $i .  '.' . $file->getClientOriginalExtension();
                array_push($data, array('name' => $name, 'post_id' => $post->id));
                $destinationPathGallery = public_path('/upload/gallery');
                $file->move($destinationPathGallery, $name);
                $i++;
            }
            Photo::insert($data);
        }

        if ($request->dates) {
            foreach ($request->dates as $date) {
                $arrayDate =  explode(" - ", $date);
                array_push($arrayDates, array('start' => $arrayDate[0], 'end' => $arrayDate[1], 'post_id' => $post->id));
            }
            Date::insert($arrayDates);
        }

        return back()->with('success', 'Post Created');
    }

    public function update(StorePostRequest $request, $id)
    {
        $data = array();
        $arrayDates = array();
        $dateIds = array();

        $input = $request->except('dates', 'multiple_photos');
        $post = Post::findOrFail($id);

        if ($post) {

            if ($request->hasFile('profile_photo')) {
                $image = $request->file('profile_photo');
                $name = time() . '.' .  $image->getClientOriginalExtension();
                $destinationPath = public_path('/upload/posts');
                $image->move($destinationPath, $name);
                $input['profile_photo'] = $name;
            }
            $input['days_json'] = $request->days_json ?? '';
            $post->update($input);

            if ($request->hasFile('multiple_photos')) {
                $i = 1;
                foreach ($request->file('multiple_photos') as $file) {
                    $name = time() .  $i .  '.' . $file->getClientOriginalExtension();
                    array_push($data, array('name' => $name, 'post_id' => $post->id));
                    $destinationPathGallery = public_path('/upload/gallery');
                    $file->move($destinationPathGallery, $name);
                    $i++;
                }
                Photo::insert($data);
            }

            if ($post->dates) {
                foreach ($post->dates as $date) {
                    array_push($dateIds, $date->id);
                }
                $delete = Date::destroy($dateIds);
            }

            if ($request->dates) {

                foreach ($request->dates as $date) {
                    $arrayDate =  explode(" - ", $date);
                    array_push($arrayDates, array('start' => $arrayDate[0], 'end' => $arrayDate[1], 'post_id' => $post->id));
                }
                Date::insert($arrayDates);
            }


            return back()->with('success', 'Post Updated');
        } else {
            return back()->with('failed', 'Post not found');
        }
    }


    public function restore(Request $request)
    {
        Post::withTrashed()->find($request->post_id)->restore();
        return back()->with('success', 'Post Restored');
    }

    public function softDelete($id)
    {
        $post = Post::find($id);
        if ($post) {
            $post->delete();
            return back()->with('success', 'Post Soft Deleted');
        }
    }

    public function galleryDelete($id)
    {
        try {

            $photo = Photo::find($id);

            $image_path =  public_path('/upload/gallery/') . $photo->name;
            if (File::exists($image_path)) {
                File::delete($image_path);
            }

            $photo->delete();
            return back()->with('success', 'Photo Deleted');
        } catch (Throwable $e) {

            return $e;
        }
    }
}
