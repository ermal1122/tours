<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Rules\MatchOldPassword;


class UserController extends Controller
{

    public function edit()
    {
        return view('user.edit');
    }

    public function update(User $user, Request $request)
    {
        $this->validate(request(), [
            'name' => 'required',
            'surname' => 'required',
        ]);

        $user->update($request->all());

        return back()->with('success', 'User Updated');
    }

    public function account()
    {
        return view('user.account');
    }

    public function purcharses()
    {
        $trips = Payment::where('user_id', Auth::user()->id)->with('post')->paginate(8);
        return view('user.purchase', compact('trips'));
    }

    public function password()
    {
        return view('user.change_password');
    }

    public function changePassword(Request $request)
    {
        $request->validate([
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => ['required'],
            'new_confirm_password' => ['same:new_password'],
        ]);

        $user = Auth::user();
        $user->password = bcrypt($request->new_password);
        $user->save();

        return back()->with('success', 'Password Updated');
    }
}
