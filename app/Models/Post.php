<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Post extends Model
{
    use HasFactory;
    use SoftDeletes;


    protected $fillable = [
        'title', 'description', 'price', 'days_json', 'duration', 'departure', 'profile_photo', 'return',
        'city', 'category_id', 'free_seats', 'acomodation', 'mobile_ticket', 'cancellation', 'confirmation',
        'languages', 'inclusions'
    ];

    public function photos()
    {
        return $this->hasMany('App\Models\Photo');
    }

    public function dates()
    {
        return $this->hasMany('App\Models\Date');
    }
    public function category()
    {
        return $this->hasOne('App\Models\Category', 'id');
    }
}
