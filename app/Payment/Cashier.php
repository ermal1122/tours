<?php

namespace App\Payment;

use App\Payment\Api\Twocheckout_Charge;
use App\Payment\Twocheckout;
use Twocheckout_Error;

class Cashier
{
    public static function pay($paymentInfo)
    {
        Twocheckout::privateKey('C61B3CE7-8373-4E60-9146-BAB8755C261D');
        Twocheckout::sellerId('250535517940');
        Twocheckout::verifySSL(false);

        try {
            $charge = Twocheckout_Charge::auth($paymentInfo, 'array');
            $this->assertEquals('APPROVED', $charge['response']['responseCode']);
        } catch (Twocheckout_Error $e) {
            $this->assertEquals('Unauthorized', $e->getMessage());
        }
    }
}
