<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\Category::create([
            'name' => 'Paketa Verore',
        ]);
        App\Models\Category::create([
            'name' => 'Paketa Dimerore',
        ]);
    }
}
