<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('description');
            $table->integer('price');
            $table->json('days_json');
            $table->string('duration');
            $table->string('profile_photo');
            $table->string('departure');
            $table->string('return');
            $table->string('city');
            $table->integer('free_seats')->nullable();
            $table->string('acomodation')->nullable();
            $table->boolean('mobile_ticket')->default(0);
            $table->integer('cancellation')->nullable();
            $table->integer('confirmation')->nullable();
            $table->string('languages')->nullable();
            $table->string('inclusions')->nullable();
            $table->integer('category_id');
            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
